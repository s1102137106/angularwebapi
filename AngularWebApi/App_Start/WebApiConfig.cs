﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Extensions;
using AngularWebApi.Models;
using System.Web.Http.Cors;

namespace AngularWebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API 設定和服務

            // Web API 路由
            config.MapHttpAttributeRoutes();

            config.EnableCors(new EnableCorsAttribute("*", "*", "*"));
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
            builder.EntitySet<Employees>("Employees");

            builder.EntitySet<Orders>("OdataOrder");
            builder.EntitySet<Customers>("Customers");
            builder.EntitySet<Shippers>("Shippers");
            builder.EntitySet<Products>("Products");
            builder.EntitySet<Categories>("Categories");
            builder.EntitySet<Suppliers>("Suppliers");
            builder.EntitySet<OrderDetails>("OdataOrderDetail");



            config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());


        }
    }
}
