﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using AngularWebApi.Models;

namespace AngularWebApi.Controllers
{
    /*
    WebApiConfig 類別可能需要其他變更以新增此控制器的路由，請將這些陳述式合併到 WebApiConfig 類別的 Register 方法。注意 OData URL 有區分大小寫。

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using AngularWebApi.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Products>("Products");
    builder.EntitySet<Categories>("Categories"); 
    builder.EntitySet<OrderDetails>("OrderDetails"); 
    builder.EntitySet<Suppliers>("Suppliers"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ProductsController : ODataController
    {
        private TSQL db = new TSQL();

        // GET: odata/Products
        [EnableQuery]
        public IQueryable<Products> GetProducts()
        {
            return db.Products;
        }

        // GET: odata/Products(5)
        [EnableQuery]
        public SingleResult<Products> GetProducts([FromODataUri] int key)
        {
            return SingleResult.Create(db.Products.Where(products => products.productid == key));
        }

        // PUT: odata/Products(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Products> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Products products = db.Products.Find(key);
            if (products == null)
            {
                return NotFound();
            }

            patch.Put(products);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductsExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(products);
        }

        // POST: odata/Products
        public IHttpActionResult Post(Products products)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Products.Add(products);
            db.SaveChanges();

            return Created(products);
        }

        // PATCH: odata/Products(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Products> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Products products = db.Products.Find(key);
            if (products == null)
            {
                return NotFound();
            }

            patch.Patch(products);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductsExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(products);
        }

        // DELETE: odata/Products(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Products products = db.Products.Find(key);
            if (products == null)
            {
                return NotFound();
            }

            db.Products.Remove(products);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/Products(5)/Categories
        [EnableQuery]
        public SingleResult<Categories> GetCategories([FromODataUri] int key)
        {
            return SingleResult.Create(db.Products.Where(m => m.productid == key).Select(m => m.Categories));
        }

        // GET: odata/Products(5)/OrderDetails
        [EnableQuery]
        public IQueryable<OrderDetails> GetOrderDetails([FromODataUri] int key)
        {
            return db.Products.Where(m => m.productid == key).SelectMany(m => m.OrderDetails);
        }

        // GET: odata/Products(5)/Suppliers
        [EnableQuery]
        public SingleResult<Suppliers> GetSuppliers([FromODataUri] int key)
        {
            return SingleResult.Create(db.Products.Where(m => m.productid == key).Select(m => m.Suppliers));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProductsExists(int key)
        {
            return db.Products.Count(e => e.productid == key) > 0;
        }
    }
}
