﻿using AngularWebApi.Models;
using AngularWebApi.ResultModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.OData;

namespace AngularWebApi.Controllers
{
    [EnableCors("*", "*", "*")]
    public class OdataOrderDetailController : ApiController
    {

        private TSQL db = new TSQL();

        [HttpPut]
        public HttpResponseMessage putOrderDetail(OrderDetail orderDetail)
        {
            //全部刪除 再新增

            db.Database.ExecuteSqlCommand(@"INSERT INTO [Sales].[OrderDetails]
                                           ([orderid]
                                           ,[productid]
                                           ,[unitprice]
                                           ,[qty]
                                           ,[discount])
                                             VALUES
                                           ({0}
                                           ,{1}
                                           ,{2}
                                           ,{3}
                                           ,{4})",
                                            orderDetail.orderid,
                                            orderDetail.productid,
                                            orderDetail.unitprice,
                                            orderDetail.qty,
                                            orderDetail.discount
                                        );


            return Request.CreateResponse(HttpStatusCode.OK, new {success = true});
        }

        [HttpDelete]
        public HttpResponseMessage deleteOrderDetail([FromODataUri] int id)
        {
            db.Database.ExecuteSqlCommand(@"DELETE FROM [Sales].[OrderDetails]
                                            Where orderid = {0}",
                                            id
                                        );


            return Request.CreateResponse(HttpStatusCode.OK, new { success = true });
        }
    }
}
