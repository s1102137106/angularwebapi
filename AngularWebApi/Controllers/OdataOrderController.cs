﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using AngularWebApi.Models;
using System.Web.Http.Cors;

namespace AngularWebApi.Controllers
{
    /*
    WebApiConfig 類別可能需要其他變更以新增此控制器的路由，請將這些陳述式合併到 WebApiConfig 類別的 Register 方法。注意 OData URL 有區分大小寫。

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using AngularWebApi.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Orders>("OdataOrder");
    builder.EntitySet<Customers>("Customers"); 
    builder.EntitySet<Employees>("Employees"); 
    builder.EntitySet<OrderDetails>("OrderDetails"); 
    builder.EntitySet<Shippers>("Shippers"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    [EnableCors("*", "*", "*")]
    public class OdataOrderController : ODataController
    {
        private TSQL db = new TSQL();

        // GET: odata/OdataOrder
        [EnableQuery]
        public IQueryable<Orders> GetOdataOrder()
        {
            return db.Orders;
        }

        // GET: odata/OdataOrder(5)
        [EnableQuery]
        public SingleResult<Orders> GetOrders([FromODataUri] int key)
        {
            return SingleResult.Create(db.Orders.Where(orders => orders.orderid == key));
        }

        // PUT: odata/OdataOrder(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Orders> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Orders orders = db.Orders.Find(key);
            if (orders == null)
            {
                return NotFound();
            }

            patch.Put(orders);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrdersExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(orders);
        }

        // POST: odata/OdataOrder
        public IHttpActionResult Post(Orders orders)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Orders.Add(orders);
            db.SaveChanges();

            return Created(orders);
        }

        // PATCH: odata/OdataOrder(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Orders> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Orders orders = db.Orders.Find(key);
            if (orders == null)
            {
                return NotFound();
            }

            patch.Patch(orders);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrdersExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(orders);
        }

        // DELETE: odata/OdataOrder(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Orders orders = db.Orders.Find(key);
            if (orders == null)
            {
                return NotFound();
            }

            db.Orders.Remove(orders);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/OdataOrder(5)/Customers
        [EnableQuery]
        public SingleResult<Customers> GetCustomers([FromODataUri] int key)
        {
            return SingleResult.Create(db.Orders.Where(m => m.orderid == key).Select(m => m.Customers));
        }

        // GET: odata/OdataOrder(5)/Employees
        [EnableQuery]
        public SingleResult<Employees> GetEmployees([FromODataUri] int key)
        {
            return SingleResult.Create(db.Orders.Where(m => m.orderid == key).Select(m => m.Employees));
        }

        // GET: odata/OdataOrder(5)/OrderDetails
        [EnableQuery]
        public IQueryable<OrderDetails> GetOrderDetails([FromODataUri] int key)
        {
            var result = db.Orders.Where(m => m.orderid == key).SelectMany(m => m.OrderDetails);
            return result;
        }

        // GET: odata/OdataOrder(5)/Shippers
        [EnableQuery]
        public SingleResult<Shippers> GetShippers([FromODataUri] int key)
        {
            return SingleResult.Create(db.Orders.Where(m => m.orderid == key).Select(m => m.Shippers));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OrdersExists(int key)
        {
            return db.Orders.Count(e => e.orderid == key) > 0;
        }
    }
}
