﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using AngularWebApi.Models;

namespace AngularWebApi.Controllers
{
    /*
    WebApiConfig 類別可能需要其他變更以新增此控制器的路由，請將這些陳述式合併到 WebApiConfig 類別的 Register 方法。注意 OData URL 有區分大小寫。

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using AngularWebApi.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Shippers>("Shippers");
    builder.EntitySet<Orders>("Orders"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ShippersController : ODataController
    {
        private TSQL db = new TSQL();

        // GET: odata/Shippers
        [EnableQuery]
        public IQueryable<Shippers> GetShippers()
        {
            return db.Shippers;
        }

        // GET: odata/Shippers(5)
        [EnableQuery]
        public SingleResult<Shippers> GetShippers([FromODataUri] int key)
        {
            return SingleResult.Create(db.Shippers.Where(shippers => shippers.shipperid == key));
        }

        // PUT: odata/Shippers(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Shippers> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Shippers shippers = db.Shippers.Find(key);
            if (shippers == null)
            {
                return NotFound();
            }

            patch.Put(shippers);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShippersExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(shippers);
        }

        // POST: odata/Shippers
        public IHttpActionResult Post(Shippers shippers)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Shippers.Add(shippers);
            db.SaveChanges();

            return Created(shippers);
        }

        // PATCH: odata/Shippers(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Shippers> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Shippers shippers = db.Shippers.Find(key);
            if (shippers == null)
            {
                return NotFound();
            }

            patch.Patch(shippers);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShippersExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(shippers);
        }

        // DELETE: odata/Shippers(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Shippers shippers = db.Shippers.Find(key);
            if (shippers == null)
            {
                return NotFound();
            }

            db.Shippers.Remove(shippers);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/Shippers(5)/Orders
        [EnableQuery]
        public IQueryable<Orders> GetOrders([FromODataUri] int key)
        {
            return db.Shippers.Where(m => m.shipperid == key).SelectMany(m => m.Orders);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ShippersExists(int key)
        {
            return db.Shippers.Count(e => e.shipperid == key) > 0;
        }
    }
}
