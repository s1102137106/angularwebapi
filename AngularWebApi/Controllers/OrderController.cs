﻿using AngularWebApi.Models;
using AngularWebApi.ResultModels;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Http.Results;

namespace AngularWebApi.Controllers
{
    public class OrderController : SuperApiController
    {
        private TSQL db = new TSQL();

        [HttpPost]
        public JsonResult<List<OrderResultModel>> OrderList(OrderConditonModel condition)
        {
            List<OrderResultModel> result = db.Database
                .SqlQuery<OrderResultModel>(@"SELECT O.OrderID ,
	                                        C.CompanyName,
	                                        O.OrderDate,
	                                        O.ShippedDate
		                                    From Sales.Orders O left join Sales.Customers C on O.custid = C.custid
                                            WHERE 
                                            (O.orderid = {0} or {0} = 0) and
                                            (C.CompanyName LIKE {1} or {1} = '') and
                                            (O.empID = {2} or {2} = '') and
                                            (O.shipperID = {3} or {3} = '') and
                                            (O.orderdate = {4} or {4} = '') and
                                            (O.shippeddate = {5} or {5} = '')
                                            ",
                                             condition.orderId,
                                             IfNullConvertToEmpty(AddLikePersent(condition.custName)),
                                             IfNullConvertToEmpty(condition.empID),
                                             IfNullConvertToEmpty(condition.shipperID),
                                             IfNullConvertToEmpty(condition.orderDate),
                                             IfNullConvertToEmpty(condition.shipperDate)
                                             ).ToList();
            return Json<List<OrderResultModel>>(result);
        }

        [HttpPut]
        public IHttpActionResult putOrderDetail(Object orderDetails)
        {

            var orderDetailList = JValue.Parse(orderDetails.ToString()).ToList();
            dynamic jOrderDetail;
            OrderDetail orderModel;
            foreach (var orderDetail in orderDetailList)
            {
                jOrderDetail = JValue.Parse(orderDetail.ToString());

                orderModel = new OrderDetail
                {
                    unitprice = Convert.ToDecimal(jOrderDetail.unitprice.ToString()),
                    qty = Convert.ToInt32(jOrderDetail.qty.ToString()),
                    orderid = Convert.ToInt32(jOrderDetail.orderid.ToString()),
                    productid = Convert.ToInt32(jOrderDetail.productid.ToString())
                };



                db.Database.ExecuteSqlCommand(@"UPDATE [Sales].[OrderDetails]
                                                SET  [unitprice] = {0}
                                                    ,[qty] = {1}
                                                WHERE [orderid] = {2} and [productid] = {3}",
                                                 orderModel.unitprice,
                                                 orderModel.qty,
                                                 orderModel.orderid,
                                                 orderModel.productid
                                            );
            }


            return Ok();
        }


    }
}
