﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace AngularWebApi.Controllers
{
    public class SuperApiController : ApiController
    {
        //將null變成空字串 
        protected string IfNullConvertToEmpty(string str)
        {
            return str == null ? string.Empty : str;
        }

        //將null變成空字串 
        protected Object IfNullConvertToEmpty(DateTime? datetime)
        {
            if (datetime == null)
            {
                return string.Empty;
            }
            return datetime;
        }

        //將字母將上%號
        protected string AddLikePersent(string str)
        {
            if (str == null)
            {
                return str;
            }
            return string.Format("{0}%", str);
        }
    }
}