﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using AngularWebApi.Models;
using AngularWebApi.ResultModels;
using System.Web.Http.Cors;

namespace AngularWebApi.Controllers
{
    /*
    WebApiConfig 類別可能需要其他變更以新增此控制器的路由，請將這些陳述式合併到 WebApiConfig 類別的 Register 方法。注意 OData URL 有區分大小寫。

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using AngularWebApi.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Employees>("Employees");
    builder.EntitySet<Orders>("Orders"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    [EnableCors("*", "*", "*")]
    public class EmployeesController : ODataController
    {
        private TSQL db = new TSQL();

        // GET: odata/Employees
        [EnableQuery]

        public IQueryable<Employees> GetEmployees()
        {
            return db.Employees;
        }



        // GET: odata/Employees(5)
        [EnableQuery]
        public SingleResult<Employees> GetEmployees([FromODataUri] int key)
        {
            return SingleResult.Create(db.Employees.Where(employees => employees.empid == key));
        }

        // PUT: odata/Employees(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Employees> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Employees employees = db.Employees.Find(key);
            if (employees == null)
            {
                return NotFound();
            }

            patch.Put(employees);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmployeesExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(employees);
        }

        // POST: odata/Employees
        public IHttpActionResult Post(Employees employees)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Employees.Add(employees);
            db.SaveChanges();

            return Created(employees);
        }

        // PATCH: odata/Employees(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Employees> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Employees employees = db.Employees.Find(key);
            if (employees == null)
            {
                return NotFound();
            }

            patch.Patch(employees);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmployeesExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(employees);
        }

        // DELETE: odata/Employees(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Employees employees = db.Employees.Find(key);
            if (employees == null)
            {
                return NotFound();
            }

            db.Employees.Remove(employees);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/Employees(5)/Employees1
        [EnableQuery]
        public IQueryable<Employees> GetEmployees1([FromODataUri] int key)
        {
            return db.Employees.Where(m => m.empid == key).SelectMany(m => m.Employees1);
        }

        // GET: odata/Employees(5)/Employees2
        [EnableQuery]
        public SingleResult<Employees> GetEmployees2([FromODataUri] int key)
        {
            return SingleResult.Create(db.Employees.Where(m => m.empid == key).Select(m => m.Employees2));
        }

        // GET: odata/Employees(5)/Orders
        [EnableQuery]
        public IQueryable<Orders> GetOrders([FromODataUri] int key)
        {
            return db.Employees.Where(m => m.empid == key).SelectMany(m => m.Orders);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EmployeesExists(int key)
        {
            return db.Employees.Count(e => e.empid == key) > 0;
        }
    }
}
