﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularWebApi.ResultModels
{
    public class OrderDetail
    {
        public int orderid { get; set; }

        public int productid { get; set; }

        public decimal unitprice { get; set; }

        public int qty { get; set; }

        public decimal discount { get; set; }
    }
}