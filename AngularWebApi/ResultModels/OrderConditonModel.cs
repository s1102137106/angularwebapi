﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularWebApi.ResultModels
{
    public class OrderConditonModel
    {
        public int orderId { get; set; }
        public string custName { get; set; }
        public string empID { get; set; }
        public string shipperID { get; set; }
        public DateTime? orderDate { get; set; }
        public DateTime? shipperDate { get; set; }
    }
}