﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularWebApi.ResultModels
{
    public class OrderResultModel
    {
            public int OrderID { get; set; }

            public string CompanyName { get; set; }

            public DateTime OrderDate { get; set; }

            public DateTime? ShippedDate { get; set; }
    }
}