﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularWebApi.ResultModels
{
    public class SelectItemModel
    {
        public string name { get; set; }
        public string value { get; set; }
    }
}